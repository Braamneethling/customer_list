# Customer_List



## Getting started

## Importing the Database
Right click Databases on left pane (Object Explorer) <br />
Click Restore Database... <br />
Choose Device, click ..., and add Customer.bak file <br />
Click OK, then OK again <br />
Run in stored Procedure script attached. <br />

## Clone
Send me your Gitlab username so i can add you as contributor as the repo is private.<br />
Clone the repo. <br />
Change the connection string in socket service <br />

## Steps

Run the Service <br />
Run the Client <br />
A data grid view will appear with specified customer details. <br />

