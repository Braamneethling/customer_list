﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Client.Interfaces;
using Client.Services;
using Microsoft.Extensions.DependencyInjection;
using System.ServiceModel.Configuration;
using Microsoft.Extensions.Hosting;

namespace Client
{
    static class Program
    {
        public static IServiceProvider ServiceProvider { get; set; }

        static IHostBuilder CreateHostBuilder()
        {
            return Host.CreateDefaultBuilder()
                .ConfigureServices((context, services) => {
                    services.AddTransient<ISocketService, SocketServices>();
                    services.AddTransient<Client>();
                });
        }

        [STAThread]
        static void Main()
        {

            var host = CreateHostBuilder().Build();
            ServiceProvider = host.Services;
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(ServiceProvider.GetRequiredService<Client>());
        }
    }
}
