﻿using System;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Windows.Forms;
using Client.Interfaces;

namespace Client
{
    public partial class Client : Form
    {
        private readonly ISocketService _socketService;
        public Client(ISocketService socketService)
        {
            _socketService = socketService;
            StartClient();
        }

        public void StartClient()
        {
            var bytes = new byte[1024];

            try
            {
                var hostInfo = _socketService.GetHostInfo();
                var sender = _socketService.GetSender(hostInfo);

                try
                {
                    sender.Connect(hostInfo.IpEndPoint);
                    var sendClientResults = _socketService.SendClient(sender, bytes);
                    var showDataGridTable = _socketService.ShowDataGridTable(sendClientResults);
                    Controls.Add(showDataGridTable);

                }
                catch (ArgumentNullException ane)
                {
                    MessageBox.Show(ane.ToString(), @"Argument Null Exception",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                catch (SocketException se)
                {
                    MessageBox.Show(se.ToString(), @"Socket Exception",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.ToString(), @"Exception",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString(), @"Exception",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

    }
}
