﻿using System.Net;
using System.Net.Sockets;
using System.Windows.Forms;
using Client.Models;

namespace Client.Interfaces
{
    public interface ISocketService
    {
        HostInfo GetHostInfo();
        Socket GetSender(HostInfo hostInfo);
        string[] SendClient(Socket sender, byte[] bytes);
        void SendServer(Socket listener, byte[] bytes, HostInfo hostInfo);
        DataGridView ShowDataGridTable(string[] sendClientResults);
    }
}