﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Client.Interfaces;
using Client.Models;

namespace Client.Services
{
    public class SocketServices : ISocketService
    {
        private const string SqlConInfo =
            "Data Source=BRAAM-PC\\SQLEXPRESS;initial catalog=Customer;trusted_connection=true";

        private const string StoredProcName = "pr_GetCustomers";
        public HostInfo GetHostInfo()
        {
            var hostInfo = new HostInfo {IpHostEntry = Dns.GetHostEntry(Dns.GetHostName())};
            hostInfo.IpAddress = hostInfo.IpHostEntry.AddressList[0];
            hostInfo.IpEndPoint = new IPEndPoint(hostInfo.IpAddress, 11001);
            return hostInfo;
        }

        public Socket GetSender(HostInfo hostInfo)
        {
            return new Socket(hostInfo.IpAddress.AddressFamily,
                SocketType.Stream, ProtocolType.Tcp);
        }

        public string[] SendClient(Socket sender, byte[] bytes)
        {
            var msg = Encoding.ASCII.GetBytes("This is a test");
            sender.Send(msg);

            var bytesRec = sender.Receive(bytes);
            var result = Encoding.ASCII.GetString(bytes, 0, bytesRec);

            var items = result.Split(' ');

            sender.Shutdown(SocketShutdown.Both);
            sender.Close();

            return items;
        }

        public void SendServer(Socket listener, byte[] bytes, HostInfo hostInfo)
        {
            listener.Bind(hostInfo.IpEndPoint);
            listener.Listen(10);
            while (true)
            {
                var handler = listener.Accept();
                byte[] data = null;

                while (true)
                {
                    handler.Receive(bytes);
                    var result = GetCustomers();
                    data = result;

                    if (data == null) continue;
                    break;

                }
                var msg = data;

                handler.Send(msg);
                handler.Shutdown(SocketShutdown.Both);
                handler.Close();
            }
        }

        public DataGridView ShowDataGridTable(string[] sendClientResults)
        {
            var dataGridView = new DataGridView();
            dataGridView.PointToClient(new Point(10, 12));
            dataGridView.Size = new Size(800, 542);
            var table = new DataTable();
            table.Columns.Add("Id");
            table.Columns.Add("Name");
            table.Columns.Add("DateOfBirth");

            table.Rows.Add(sendClientResults[0], sendClientResults[1], sendClientResults[2]);
            dataGridView.DataSource = table;

            return dataGridView;
        }

        private static byte[] GetCustomers()
        {
            using (var con = new SqlConnection(SqlConInfo))
            {
                using (var cmd = new SqlCommand(StoredProcName, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@DateOfBirth", SqlDbType.DateTime).Value = "2008-10-20 00:00:00.000";
                    con.Open();
                    using (var reader = cmd.ExecuteReader())
                    {
                        if (!reader.Read()) return null;
                        var id = (int)reader[0];
                        var name = (string)reader[1];
                        var dateOfBirth = (DateTime)reader[2];
                        var data = Encoding.ASCII.GetBytes($"{id} {name} {dateOfBirth.ToShortDateString()}");
                        return data;

                    }
                }

            }
        }
    }
}
