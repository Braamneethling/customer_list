﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Client.Models
{
    public class HostInfo
    {
        public IPHostEntry IpHostEntry { get; set; }
        public IPAddress IpAddress { get; set; }
        public IPEndPoint IpEndPoint { get; set; }
    }
}
