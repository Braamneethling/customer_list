﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Windows.Forms;
using Client.Interfaces;

namespace Server
{
    public partial class ServerForm : Form
    {
        private readonly ISocketService _socketService;
        public ServerForm(ISocketService socketService)
        {
            _socketService = socketService;
            StartListening();
        }

        public void StartListening()
        {
            var bytes = new Byte[1024];
            var host = _socketService.GetHostInfo();
            var listener = _socketService.GetSender(host);
            try
            {
                _socketService.SendServer(listener, bytes, host);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString(), @"Exception",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
