﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Windows.Forms;
using Client.Interfaces;
using Client.Services;
using Microsoft.Extensions.Hosting;

namespace Server
{
    static class Program
    {
        public static IServiceProvider ServiceProvider { get; set; }

        static IHostBuilder CreateHostBuilder()
        {
            return Host.CreateDefaultBuilder()
                .ConfigureServices((context, services) => {
                    services.AddTransient<ISocketService, SocketServices>();
                    services.AddTransient<ServerForm>();
                });
        }

        [STAThread]
        static void Main()
        {

            var host = CreateHostBuilder().Build();
            ServiceProvider = host.Services;
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(ServiceProvider.GetRequiredService<ServerForm>());
        }
    }
}
